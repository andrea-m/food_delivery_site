jQuery(document).ready(function() {

    let navOffset = jQuery("#shopping_cart").offset().top;
    let shopping_cart_width = jQuery('#restaurant_logo').innerWidth();

    jQuery("#shopping_cart").wrap('<div id="shopping_cart_placeholder"></div>');
    jQuery("#shopping_cart_placeholder").height(jQuery("#shopping_cart").outerHeight());

    // jQuery("#shopping_cart").wrapInner('<div id="shopping_cart_inner"></div>');
    // jQuery("#shopping_cart_inner").wrapInner('<div id="shopping_cart_inner_most"></div>');
    jQuery(window).resize(function() {
        shopping_cart_width = jQuery('#restaurant_logo').innerWidth();
        document.getElementById('shopping_cart').style.width = shopping_cart_width + 'px';
    });
    jQuery(window).scroll(function() {
        let scrollPos = jQuery(window).scrollTop();
        if(scrollPos >= navOffset) {
            jQuery("#shopping_cart").addClass("fixed");
            document.getElementById('shopping_cart').style.width = shopping_cart_width + 'px';
        }
        else {
            jQuery("#shopping_cart").removeClass("fixed");
        }

    });


});