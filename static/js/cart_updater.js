// Delete item
let removeCartItemButtons = document.getElementsByClassName('btn-outline-danger')
for(let i = 0; i < removeCartItemButtons.length; i++) {
    let button = removeCartItemButtons[i]
    button.addEventListener('click', removeCartItem)
}


// Modify quantity
let quantityInputs = document.getElementsByClassName('dish-item-qty')
for(let i = 0; i < quantityInputs.length; i++) {
    let input = quantityInputs[i]
    input.addEventListener('change', quantityChanged)
}

// Add item
let dishItemButtons = document.getElementsByClassName('btn-dish-item')
for(let i = 0; i < dishItemButtons.length; i++) {
    let button = dishItemButtons[i]
    button.addEventListener('click', dishItemClicked)
}

// Click on the purchase button
// document.getElementsByClassName('btn-purchase')[0].addEventListener('click', purchaseClicked)
// function purchaseClicked() {
//     alert('Acquisto confermato')
//     let cartItems = document.getElementsByClassName('cart-items')[0]
//     while(cartItems.hasChildNodes()) {
//         cartItems.removeChild(cartItems.firstChild)
//     }
//     updateCartTotal()
// }

function removeCartItem(event) {
    let buttonClicked = event.target
    let elementToRemove = buttonClicked.parentElement.parentElement.parentElement
    let elementToRemove2 = buttonClicked.parentElement.parentElement.parentElement.parentElement
    if(elementToRemove.className == 'row cart-row') {
        elementToRemove.remove()
    } else {
        elementToRemove2.remove()
    }
    updateCartTotal()
}

function quantityChanged(event) {
    let input = event.target
    if(isNaN(input.value) || input.value <= 0) {
        input.value = 1
    }
    updateCartTotal()
}

function dishItemClicked(event) {

    let button = event.target
    let shopItem = button.parentElement
    let name = shopItem.getElementsByClassName('dish-name')[0].innerText
    let price = shopItem.getElementsByClassName('dish-price')[0].innerText
    let description = shopItem.getElementsByClassName('dish-description')[0].innerText
    addItemToCart(name, price, description)
}

function addItemToCart(name, price, description) {
    if(!isCartOfThisRestaurant && elements.length == 0 && cartElementsNumber != 0) {
        confirmation = confirm('Gli elementi aggiunti precedentemente al carrello verranno rimossi.')
        console.log('elements size: ' + elements.length.toString())
        if(!confirmation) {
            return
        }
    }
    let cartRow = document.createElement('div')
    // cartRow.classList.add('cart-row')
    let cartItems = document.getElementsByClassName('cart-items')[0]
    let cartItemNames = cartItems.getElementsByClassName('product-name')
    for(let i = 0; i < cartItemNames.length; i++) {
        if(cartItemNames[i].innerText == name) {
            alert('Questo elemento è già stato aggiunto al carrello!')
            return
        }
    }
    let cartRowContents = `
        <div class="row cart-row">
            <div class="col-4 col-sm-4 col-md-4">
                <div class="quantity">
                    <!--                        <input type="button" value="+" class="dish-item-plus">-->
                    <input type="number" value="1" class="dish-item-qty"
                           size="4">
                    <!--                        <input type="button" value="-" class="dish-item-minus">-->
                </div>
            </div>
            <div class="col-4 text-sm-center col-sm-4 text-md-left col-md-4">
                <p class="product-name">${name}</p>
<!--                <p>
                    <small>${description}</small>
                </p>
-->
            </div>
            <div class="col-4 col-sm-4 text-sm-center col-md-4 text-md-right row">
                <div class="col-8 col-sm-8 col-md-8 text-md-right" style="padding-top: 5px">
                 <!--   <script class="dish-original-price">let dishPrice = ${price}</script> -->
                    <p class="dish-item-price" data-original-price="${price}">${price}</p>
                </div>
                <div class="col-2 col-sm-2 col-md-2 text-right">
                    <button type="button" class="btn btn-outline-danger btn-xs">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                </div>
                <hr>
            </div>
       </div>
    `
    cartRow.innerHTML = cartRowContents
    cartItems.append(cartRow)
    cartRow.getElementsByClassName('btn-outline-danger')[0].addEventListener('click', removeCartItem)
    cartRow.getElementsByClassName('dish-item-qty')[0].addEventListener('change', quantityChanged)
    updateCartTotal()
}

let elements = []
function updateCartTotal() {
    console.log('cart button!! ', cart_button)
    elements = []
    let cartItemContainer = document.getElementsByClassName('cart-items')[0]
    let cartRows = cartItemContainer.getElementsByClassName('cart-row')
    let deliveryPriceElement = document.getElementsByClassName('delivery-price')[0].innerHTML
    let deliveryPrice = parseFloat(deliveryPriceElement.replace('Consegna: ', '').replace('€', ''))
    let total = 0.0
    for(let i = 0; i < cartRows.length; i++) {
        let cartRow = cartRows[i]
        let priceElement = cartRow.getElementsByClassName('dish-item-price')[0]
        let originalPriceElement = priceElement.getAttribute("data-original-price")
        let element = cartRow.getElementsByClassName('product-name')[0].innerHTML
        console.log('element:')
        console.log(element)
        let quantityElement = cartRow.getElementsByClassName('dish-item-qty')[0].value
        console.log(elements)
        let price = parseFloat(originalPriceElement.replace('€', ''))
        elements.push({
            'dish_name'     : element,
            'dish_quantity' : quantityElement,
            'price'         : price
        })
        let quantity = quantityElement
        let subtotal = price*quantity
        priceElement.innerText = subtotal + '€'
        total = total + subtotal
    }

    cart_total_btn = document.getElementById('shopping_cart_btn')
    purchase_btn = document.getElementById('btn_purchase')
    if(total != 0) {
        total = total + deliveryPrice
        total = Math.round(total * 100) / 100
        if(purchase_btn != null && cart_total_btn != null) {
            purchase_btn.classList.remove('disabled')
            cart_total_btn.classList.remove('disabled')
        }
    } else {
        total = 0.0
        if(purchase_btn != null && cart_total_btn != null) {
            purchase_btn.classList.add('disabled')
            cart_total_btn.classList.add('disabled')
        }
    }
    document.getElementsByClassName('cart-total-price')[0].innerHTML = total + '€'
    if(cart_total_btn != null && cart_button == true) {
        cart_total_btn.innerHTML = '<i class="fas fa-shopping-cart mr-2"></i>' + total + '€'
    }

    console.log('Numero elementi')
    console.log(elements.length)
}

