from django_tables2 import Column, TemplateColumn, tables


class CustomerOrdersTable(tables.Table):

    class Meta:
        template_name = "django_tables2/bootstrap4.html"
        attrs = {"class": "table table-striped table-bordered sortable",
                 "data-toggle": "table"
                 }

    restaurant_name = Column(accessor='restaurant.name', verbose_name="Nome ristorante", orderable=False)
    timestamp = Column(verbose_name='Data ordine', orderable=False)
    details = TemplateColumn(
        exclude_from_export=False,
        template_name='custom_users/django_tables_templates/table_customer_order_details.html',
        orderable=False,
        verbose_name='Dettagli',
        attrs={
            "th": {'style':'text-align: center;'},
            "td": {"align": "center"}
        }
    )
    modify = TemplateColumn(
        exclude_from_export=False,
        template_name='custom_users/django_tables_templates/table_order_modify.html',
        orderable=False,
        verbose_name='Modifica ordini',
        attrs={
            "th": {'style':'text-align: center;'},
            "td": {"align": "center"}
        }
    )
    cancel = TemplateColumn(
        exclude_from_export=False,
        template_name='custom_users/django_tables_templates/table_order_cancel.html',
        orderable=False,
        verbose_name='Disdici ordini',
        attrs={
            "th": {'style':'text-align: center;'},
            "td": {"align": "center"}
        }
    )

class CustomerAcceptedOrdersTable(tables.Table):

    class Meta:
        template_name = "django_tables2/bootstrap4.html"
        attrs = {"class": "table table-striped table-bordered sortable",
                 "data-toggle": "table"
                 }

    restaurant_name = Column(accessor='restaurant.name', verbose_name="Nome ristorante")
    timestamp = Column(verbose_name='Data ordine')
    arrival_time = Column(verbose_name='Arrivo stimato')
    details = TemplateColumn(
        exclude_from_export=False,
        template_name='custom_users/django_tables_templates/table_customer_order_details.html',
        orderable=False,
        verbose_name='Dettagli',
        attrs={
            "th": {'style':'text-align: center;'},
            "td": {"align": "center"}
        }
    )
    delete = TemplateColumn(
        exclude_from_export=False,
        template_name='custom_users/django_tables_templates/table_order_remove.html',
        orderable=False,
        verbose_name='Elimina ordini',
        attrs={
            "th": {'style':'text-align: center;'},
            "td": {"align": "center"}
        }
    )
