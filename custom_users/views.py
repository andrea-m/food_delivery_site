import json

from django.contrib import messages
from django.contrib.auth import views as auth_views
from django.core.mail import send_mail
from django.http import HttpResponseForbidden, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy, reverse, resolve
from django.views.generic import CreateView
from django_tables2 import RequestConfig

from custom_users.forms import CustomerUserCreationForm, RestaurateurUserCreationForm
from custom_users.tables import CustomerOrdersTable, CustomerAcceptedOrdersTable
from food_delivery.email_settings import EMAIL_HOST_USER
from restaurants.forms import PaymentForm
from restaurants.models import Order, Dish, OrderedDish


def customer_home(request):
    """
    Renders the home dedicated to the customer
    """
    user_name = request.user.first_name
    cart_total = request.session.get('cart_elements', {}).get('total', 0.0)
    context = {'user_name': user_name, 'cart_total': cart_total}
    return render(request, 'home.html', context)


def restaurateur_home(request):
    """
    Renders the home dedicated to the customer
    """
    user_name = request.user.first_name
    cart_total = request.session.get('cart_elements', {}).get('total', 0.0)
    context = {'user_name': user_name, 'cart_total': cart_total}
    return render(request, 'custom_users/restaurateur_home.html', context)


def customer_orders_managing(request):
    """
    Shows the customer the orders that have been made and gives him the possibility to modify or cancel them
    """
    customers_orders = Order.objects.filter(customer=request.user).filter(deleted_by_customer=False).filter(accepted=False)
    orders_number = len(customers_orders)
    table = CustomerOrdersTable(customers_orders)
    cart_total = request.session.get('cart_elements', {}).get('total', 0.0)
    request.session['details_previous'] = resolve(request.get_full_path()).url_name
    RequestConfig(request).configure(table)
    context = {'table': table, 'user_name': request.user.first_name, 'orders_number': orders_number,
               'cart_total': cart_total}
    return render(request, 'custom_users/customer_orders_managing.html', context)


def customer_accepted_orders(request):
    """
    Shows the customer the accepted orders and gives him the possibility to delete them
    """
    customers_orders = Order.objects.filter(customer=request.user).filter(deleted_by_customer=False).filter(accepted=True)
    orders_number = len(customers_orders)
    table = CustomerAcceptedOrdersTable(customers_orders)
    cart_total = request.session.get('cart_elements', {}).get('total', 0.0)
    request.session['details_previous'] = resolve(request.get_full_path()).url_name
    RequestConfig(request).configure(table)
    context = {'table': table, 'user_name': request.user.first_name, 'orders_number': orders_number,
               'cart_total': cart_total}
    return render(request, 'custom_users/customer_accepted_orders_managing.html', context)


def customer_order_details(request, id):
    """
    Shows the details of the order selected by the customer
    """
    order = Order.objects.get(id=id)
    ordered_dishes_ref = OrderedDish.objects.filter(order=order)
    ordered_dishes = list()
    for ordered_dish in ordered_dishes_ref:
        dish = Dish.objects.get(id=ordered_dish.dish.id)
        dishes_dict = dict()
        dishes_dict['name'] = dish.name
        dishes_dict['quantity'] = ordered_dish.quantity
        dishes_dict['price'] = dish.price * ordered_dish.quantity
        ordered_dishes.append(dishes_dict)
    details_previous = request.session.get('details_previous', 'customer-orders-managing')
    cart_total = request.session.get('cart_elements', {}).get('total', 0.0)
    previous = 'custom_users:' + details_previous.__str__()
    context = {'order': order, 'ordered_dishes': ordered_dishes, 'user_name': request.user.first_name,
               'previous': previous, 'cart_total': cart_total}
    return render(request, 'custom_users/customer_order_details.html', context)


def customer_modify_order(request, id=None, template_name='custom_users/customer_modify_order.html'):
    """
    Handles the order modification. Only the parameters of the Order objects, not the OrderedDish ones.
    """
    if id:
        order = get_object_or_404(Order, pk=id)
        request.session['order_id'] = order.id
        if order.customer != request.user:
            return HttpResponseForbidden()
    else:
        return

    form = PaymentForm(request.POST or None, instance=order)
    if request.POST and form.is_valid():
        form.save()

        # Save was successful, so redirect to another page
        messages.success(request, 'Ordine modificato.')
        redirect_url = reverse('custom_users:customer-orders-managing')
        return redirect(redirect_url)

    ordered_dishes = OrderedDish.objects.filter(order=order)
    cart_total = request.session.get('cart_elements', {}).get('total', 0.0)
    context = {'ordered_dishes': ordered_dishes, 'form': form, 'order': order, 'user_name': request.user.first_name,
               'cart_total': cart_total}
    return render(request, template_name, context)


def customer_cancel_order(request, id):
    """
    Handles the cancellation of an order by the customer
    """
    order = Order.objects.get(id=id)

    # Sends a notification email to the restaurateur
    cart_elements = OrderedDish.objects.filter(order=order)
    delivery_price = order.price
    message = "L'ordine n°" + order.id.__str__() + " effettuato dall'utente " + order.customer.email.__str__() + \
              " e comprendente:\n\n"
    for element in cart_elements:
        quantity = element.quantity
        name = element.dish.name
        price = element.dish.price
        message += '- ' + quantity.__str__() + \
                   ' ' + name.__str__() + ',\n'
    message += "\nper un totale di € " + delivery_price.__str__()
    message += "è stato disdetto.\n"
    send_mail(
        "Disdetta ordine",
        message,
        EMAIL_HOST_USER,
        [order.restaurant.restaurateur.email],
        fail_silently=False,
    )

    order.delete()
    messages.info(request, 'Ordine disdetto.')
    return redirect(request.META.get('HTTP_REFERER'))


def customer_order_remove(request, id):
    """
    Handles the orders' removal
    """
    order = Order.objects.get(id=id)
    order.deleted_by_customer = True
    order.save()
    messages.success(request, 'Ordine eliminato.')
    return redirect(request.META.get('HTTP_REFERER'))


# ------------------ User -------------------

class CustomerUserCreateView(CreateView):
    """
    Handles the creation of a new customer user
    """
    form_class = CustomerUserCreationForm
    template_name = 'registration/create_custom_user.html'
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        response = super(CustomerUserCreateView, self).form_valid(form)
        self.object.save()

        messages.info(self.request, 'Nuovo utente creato con successo')
        return redirect('custom_users:login')


class RestaurateurUserCreateView(CreateView):
    """
    Handles the creation of a new restaurateur
     user
    """
    form_class = RestaurateurUserCreationForm
    template_name = 'registration/create_custom_user.html'
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        response = super(RestaurateurUserCreateView, self).form_valid(form)
        self.object.save()

        messages.info(self.request, 'Nuovo utente ristoratore creato con successo')
        return redirect('custom_users:login')


class CustomLoginView(auth_views.LoginView):
    """
    Handles the authentication of the users redirecting to the payment page if they tried to access it without
    authentication otherwise redirecting to the homepage and cleaning the shopping cart.
    """

    def get_context_data(self, **kwargs):
        context = super(CustomLoginView, self).get_context_data(**kwargs)
        context['previous_url'] = self.request.META.get('HTTP_REFERER')
        return context
    def form_valid(self, form):

        url = self.request.GET.get("next")
        if url and form.get_user().is_restaurateur:
            messages.error(self.request, 'Solo gli utenti registrati come clienti possono accedere al pagamento.')
            return redirect(url)
        else:
            return super().form_valid(form)

    def get_success_url(self):
        url = self.request.GET.get("next")
        messages.success(self.request, 'Login effettuato con successo')
        if url:
            if self.request.user.is_customer:
                return reverse('restaurants:payment')
        else:
            self.request.session['cart_elements'] = {}
            return reverse('home')


# -------------------- AJAX ------------------

def ajax_get_cart_data(request):
    """
    Gets the cart data from the Ajax function in customer_modify_order.html.
    The customer can modify the order only if the restaurateur hasn't accepted it, so there is a locking system
    that doesn't allow the customer to make modifications if the order is locked (that meas the restaurateur is
    accepting it in the same moment).
    If the order isn't locked all the data is checked and if the number of dishes ordered is changed the restaurateur
    is notified.
    """
    order_id = request.session.get('order_id', '')
    order = Order.objects.get(id=order_id)

    # If the restaurateur is accepting the order, the order is locked and the customer can't modify it
    if order.lock:
        correct = False
        data = {
            'correct': correct,
            'message': 'Si è verificato un accesso simultaneo alla modifica di questo ordine.'
        }
        return JsonResponse(data)
    else:
        order.lock = True
        order.save()

    cart_elements_str = request.POST.get('elements', {})
    cart_elements = json.loads(cart_elements_str)

    # Ajax data check
    correct = True
    equal = True
    for element in cart_elements:
        dishes = Dish.objects.filter(restaurant=order.restaurant)
        # Checks if the name of the dish is present in the db
        if len(dishes.filter(name=element['dish_name'])) == 0:
            correct = False
            break
        dish = Dish.objects.filter(name=element['dish_name']).get(restaurant=order.restaurant)
        # Checks if the dish is available
        if not dish.availability:
            correct = False
            break
        # Checks if the price corresponds
        if not dish.price == element['price']:
            correct = False
            break
        ordered_dish = OrderedDish.objects.filter(order=order).get(dish=dish)
        # Checks if any changes have been made
        if element['dish_quantity'] != ordered_dish.quantity:
            equal = False
        # Saves the new ordered dish if all checks are passed
        if correct == True and equal == False:
            ordered_dish.quantity = element['dish_quantity']
            ordered_dish.save()

    # Saves the order with the updated price if the checks are passed
    if correct == True and equal == False:
        ordered_dishes = OrderedDish.objects.filter(order=order)
        total = 0.0
        for dish in ordered_dishes:
            total += int(dish.quantity) * float(dish.dish.price)
        total += float(order.restaurant.delivery_price)
        order.price = total

        # Sends a notification email to the restaurateur
        cart_elements = OrderedDish.objects.filter(order=order)
        delivery_price = order.price
        message = "L'ordine n°" + order.id.__str__() + " effettuato dall'utente " + order.customer.email.__str__() + \
                  " è stato modificato e ora comprende:\n\n"
        for element in cart_elements:
            quantity = element.quantity
            name = element.dish.name
            price = element.dish.price
            message += '- ' + quantity.__str__() + \
                       ' ' + name.__str__() + ',\n'
        message += "\nper un totale di € " + delivery_price.__str__() + "."
        send_mail(
            "Modifica ordine",
            message,
            EMAIL_HOST_USER,
            [order.restaurant.restaurateur.email],
            fail_silently=False,
        )
    order.lock = False
    order.save()

    data = {
        'correct':  correct,
        'message': 'I dati inseriti nel carrello non sono corretti!'
    }
    return JsonResponse(data)
