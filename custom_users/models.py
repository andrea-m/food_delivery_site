from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils.translation import gettext_lazy as _

class CustomUserManager(BaseUserManager):
    def create_user(self, email, first_name, password=None, is_active=False, is_customer=False, is_restaurateur=False, is_admin=False):
        if not email:
            raise ValueError("Users must have an email address")

        user = self.model(
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.first_name         = first_name
        user.is_active          = is_active
        user.is_customer        = is_customer
        user.is_restaurateur    = is_restaurateur
        user.is_admin           = is_admin
        user.save(using=self.db)
        return user

    def create_superuser(self, email, first_name, password=None):
        user = self.create_user(
            email,
            first_name,
            password=password,
            is_active=True,
            is_admin=True,
        )
        user.save(using=self._db)
        return user

class CustomUser(AbstractBaseUser):
    email           = models.EmailField(verbose_name="email", max_length=60, unique=True)
    first_name      = models.CharField(max_length=30)
    is_admin        = models.BooleanField(default=False)
    is_active       = models.BooleanField(default=True)
    is_customer     = models.BooleanField(default=False)
    is_restaurateur = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name']

    objects = CustomUserManager()

    def __str__(self):
        return self.email

    @property
    def has_profile(self):
        try:
            assert self.profile
            return True
        except ObjectDoesNotExist:
            return False

    @property
    def is_superuser(self):
        return self.is_admin

    @property
    def is_staff(self):
        return self.is_admin

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return self.is_admin

    class Meta:
        verbose_name = _('Food Delivery user')
        verbose_name_plural = _('Food Delivery users')


class Address(models.Model):
    user            = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    street          = models.CharField(max_length=100)
    house_number    = models.CharField(max_length=5)
    zipcode         = models.CharField(max_length=6)
    city            = models.CharField(max_length=50)
    province        = models.CharField(max_length=2)

    class Meta:
        verbose_name = _("Address")
        verbose_name_plural = _("Users' addresses")