from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction
from custom_users.models import CustomUser


class CustomerUserCreationForm(UserCreationForm):
    email = forms.EmailField(required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs['placeholder'] = 'Nome'
        self.fields['email'].widget.attrs['placeholder'] = 'example@example.com'
        self.helper = FormHelper()
        self.helper.add_input(
            Submit('submit', 'Crea un account', css_class='btn btn-success')
        )

    class Meta:
        model = CustomUser
        fields = (
            'first_name',
            'email',
            'password1',
            'password2',
        )
        labels = {
            'first_name': 'Nome',
            'email'     : 'email',
            'password1' : 'Password',
            'password2' : 'Ripeti password'
        }

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_customer = True
        user.save()
        return user


class RestaurateurUserCreationForm(UserCreationForm):
    email = forms.EmailField(required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs['placeholder'] = 'name'
        self.fields['email'].widget.attrs['placeholder'] = 'example@example.com'
        self.fields['password1'].widget.attrs['placeholder'] = 'choose your password wisely'
        self.fields['password2'].widget.attrs['placeholder'] = 'repeat your chosen password'
        self.helper = FormHelper()
        self.helper.add_input(
            Submit('submit', 'Crea un account', css_class='btn btn-success')
        )

    class Meta:
        model = CustomUser
        fields = (
            'first_name',
            'email',
            'password1',
            'password2',
        )

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_restaurateur = True
        user.save()
        return user
