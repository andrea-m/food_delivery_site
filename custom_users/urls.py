from django.contrib.auth import views as auth_views
from django.urls import path
from custom_users import views

app_name = 'custom_users'

urlpatterns = [
    path('customer_home/', views.customer_home, name='customer-home'),
    path('restaurateur_home/', views.restaurateur_home, name='restaurateur-home'),
    path('customer_users/register/', views.CustomerUserCreateView.as_view(), name='register-customer-user'),

    path('restaurateur_users/register/', views.RestaurateurUserCreateView.as_view(), name='register-restaurateur-user'),
    path('login/', views.CustomLoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),

    path('customer_orders_managing/', views.customer_orders_managing, name='customer-orders-managing'),
    path('customer_accepted_orders_managing/', views.customer_accepted_orders, name='customer-accepted-orders'),
    path('<str:id>/customer_order_details/', views.customer_order_details, name='customer-order-details'),
    path('<str:id>/customer_modify_order/', views.customer_modify_order, name='customer-modify-order'),
    path('<str:id>/customer_cancel_order/', views.customer_cancel_order, name='customer-cancel-order'),
    path('<str:id>/customer_order_remove/', views.customer_order_remove, name='customer-order-remove'),

    path('modify_order/ajax_get_cart_data/', views.ajax_get_cart_data, name='ajax-get-cart-data'),

]