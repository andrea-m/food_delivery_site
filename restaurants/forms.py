from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms

from custom_users.models import Address
from restaurants.models import Restaurant, Order, Dish


class RestaurantForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['category'].widget.attrs['placeholder'] = 'Es: Trattoria, Fast food, Braceria'
        self.helper = FormHelper()
        self.helper.add_input(
            Submit('submit', 'Invia', css_class='btn btn-success')
        )

    class Meta:
        model = Restaurant
        fields = (
            'logo',
            'name',
            'category',
            'delivery_price',
            'street',
            'house_number',
            'zipcode',
            'city',
            'province',
        )
        labels = {
            'logo':             'Logo',
            'name':             'Nome del ristorante',
            'category':         'Categoria',
            'delivery_price':   'Prezzo di spedizione',
            'street':           'Via',
            'house_number':     'Numero civico',
            'zipcode':          'CAP',
            'city':             'Città',
            'province':         'Provincia',
        }


class DishForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(
            Submit('submit', 'Invia', css_class='btn btn-success')
        )

    class Meta:
        model = Dish
        fields = (
            'img',
            'name',
            'availability',
            'description',
            'price',
            'preparation_time',
        )
        labels = {
            'img':              'Immagine del piatto',
            'name':             'Nome del piatto',
            'availability':     'Dispponibilità',
            'description':      'Descrizione',
            'price':            'Prezzo',
            'preparation_time': 'Tempo di preparazione',
        }


class PaymentForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(
            Submit('submit', 'Invia', css_class='btn btn-success payment-button')
        )

    class Meta:
        model = Order
        fields = (
            'customer_floor_flat_nb',
            'customer_number',
            'instructions',
            'cc_number',

        )
        labels = {
            'customer_floor_flat_nb':   'Piano e interno',
            'customer_number':          'Numero di telefono',
            'instructions':             'Istruzioni per il fattorino',
            'cc_number':                'Numero carta',
        }

