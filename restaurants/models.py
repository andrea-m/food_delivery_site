from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from custom_users.models import CustomUser, Address

class Restaurant(models.Model):
    restaurateur        = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    # Information
    logo                = models.ImageField(upload_to='restaurants_logos/')
    name                = models.CharField(max_length=30)
    category            = models.CharField(max_length=30)
    delivery_price      = models.DecimalField(max_digits=4, decimal_places=2)
    # Address
    street              = models.CharField(max_length=100)
    house_number        = models.CharField(max_length=5)
    zipcode             = models.CharField(max_length=6)
    city                = models.CharField(max_length=50)
    province            = models.CharField(max_length=2)

    class Meta:
        unique_together = ('restaurateur', 'name',)

class Dish(models.Model):
    restaurant          = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    name                = models.CharField(max_length=100)
    img                 = models.ImageField(upload_to='dishes_photos/')
    availability        = models.BooleanField(default=False)
    description         = models.TextField(max_length=200)
    price               = models.DecimalField(max_digits=6, decimal_places=2)
    preparation_time    = models.IntegerField(default=10)

    class Meta:
        unique_together = ('restaurant', 'name',)

    def get_price(self):
        return self.price.__str__()+'€'

    def get_availability(self):
        if self.availability == True:
            return 'Disponibile'
        else:
            return 'Non disponibile'

    def get_preparation_time(self):
        return self.preparation_time.__str__()+' minuti'

class Order(models.Model):
    customer                = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    customer_address        = models.ForeignKey(Address, on_delete=models.CASCADE)
    customer_floor_flat_nb  = models.CharField(max_length=150, blank=True)
    customer_number         = models.CharField(max_length=15)
    instructions            = models.CharField(max_length=250, blank=True)
    cc_number               = models.CharField(max_length=16)
    restaurant              = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    timestamp               = models.CharField(max_length=50)
    delivery_time           = models.CharField(max_length=50)
    arrival_time            = models.CharField(max_length=50)
    price                   = models.DecimalField(max_digits=10, decimal_places=2, default=0.00, editable=False)
    accepted                = models.BooleanField(default=False)
    done                    = models.BooleanField(default=False)
    deleted_by_customer     = models.BooleanField(default=False)
    deleted_by_restaurateur = models.BooleanField(default=False)
    lock                    = models.BooleanField(default=False)

    def get_order_price(self):
        return self.price.__str__() + '€'

class OrderedDish(models.Model):
    order                   = models.ForeignKey(Order, on_delete=models.CASCADE)
    dish                    = models.ForeignKey(Dish, on_delete=models.CASCADE)
    quantity                = models.IntegerField(default=1)

class Review(models.Model):
    restaurant              = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    reviewer                = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    reviewer_name           = models.CharField(max_length=30)
    rating                  = models.PositiveIntegerField(default=1, validators=[MinValueValidator(1), MaxValueValidator(5)])
    comment                 = models.TextField(max_length=500)