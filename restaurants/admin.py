from django.contrib import admin
from .models import Restaurant, Dish, Review, OrderedDish, Order

admin.site.register(Restaurant)
admin.site.register(Dish)
admin.site.register(Order)
admin.site.register(OrderedDish)
admin.site.register(Review)
