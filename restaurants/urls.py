from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from django.template.context_processors import static
from django.urls import path, include

from restaurants import views
from django.conf import settings
from django.conf.urls.static import static

app_name = 'restaurants'

urlpatterns = [
    path('add_restaurant/', views.RestaurantCreateView.as_view(), name='add-restaurant'),
    path('remove_restaurant/<str:id>/', views.remove_restaurant, name='remove-restaurant'),
    path('<str:restaurant_id>/add_dish/', views.DishCreateView.as_view(), name='add-dish'),

    path('<str:pk>/', views.restaurants, name='restaurant-page'),
    path('restaurateur/restaurants_managing/', views.restaurants_managing, name='restaurants-managing'),
    path('<str:id>/modify_restaurant/', views.modify_restaurant, name='modify-restaurant'),

    path('<str:restaurant_id_order>/orders_managing/', views.orders_managing, name='orders-managing'),
    path('<str:restaurant_id_order>/accepted_orders_managing/', views.accepted_orders_managing, name='accepted-orders-managing'),
    path('<str:restaurant_id_order>/done_orders_managing/', views.done_orders_managing, name='done-orders-managing'),
    path('<str:id>/orders_details/', views.order_details, name='orders-details'),
    path('order_done/<str:id>/', views.order_done, name='order-done'),
    path('order_accept/<str:id>/', views.order_accept, name='order-accept'),
    path('remove_order/<str:id>/', views.remove_order, name='remove-order'),
    path('checkout/payment/', views.Payment.as_view(), name='payment'),

    path('<str:id>/menu_managing/', views.menu_managing, name='menu-managing'),
    path('<str:id>/modify_dish/', views.modify_dish, name='modify-dish'),
    path('<str:id>/remove_dish/', views.remove_dish, name='remove-dish'),

    path('checkout/ajax_get_cart_data/', views.ajax_get_cart_data, name='ajax-get-cart-data'),
    path('', views.ajax_get_rating_data, name='ajax-get-rating-data'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

