from datetime import datetime
import pytz
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase, Client
from django.urls import reverse
from custom_users.models import CustomUser, Address
from restaurants.models import Restaurant, Dish, Order

class OrdersView(TestCase):

    def setUp(self):
        """
        Setup of the objects used for testing
        """
        self.client = Client()
        self.restaurateur = CustomUser.objects.create(
            first_name='Mario',
            email='restaurateur@test.it',
        )
        self.restaurateur.set_password('TestUser1')
        self.restaurateur.save()
        self.customer = CustomUser.objects.create_user(
            first_name='Luca',
            email='customer@test.it',
            password='TestUser2',
        )
        self.restaurant = Restaurant.objects.create(
            restaurateur=self.restaurateur,
            logo=SimpleUploadedFile(
                name='wine-room-dari-ristorante.jpg',
                content=open('static/img/wine-room-dari-ristorante.jpg', 'rb').read(),
                content_type='image/jpg'
            ),
            name='Moretto',
            category='Ristorante',
            delivery_price=2.5,
            street='Via Roma',
            house_number='1000',
            zipcode='00100',
            city='Roma',
            province='RM',
        )
        self.restaurant.save()
        self.dish = Dish.objects.create(
            restaurant=self.restaurant,
            name='Carbonara',
            img=SimpleUploadedFile(
                name='wine-room-dari-ristorante.jpg',
                content=open('static/img/wine-room-dari-ristorante.jpg', 'rb').read(),
                content_type='image/jpg'
            ),
            availability=True,
            description='Pasta alla carbonara',
            price=7,
            preparation_time=10,
        )
        self.dish.save()
        self.customer_address = Address.objects.create(
            user=self.customer,
            street='Via Roma',
            house_number='1001',
            zipcode='00100',
            city='Roma',
            province='RM',
        )
        self.customer_address.save()
        tz = pytz.timezone('Europe/Berlin')
        self.now = datetime.now(tz)

    def test_orders_managing_with_no_orders(self):
        """
        orders_managing() shouldn't retrieve objects if they haven't been created
        """
        self.client.login(email='restaurateur@test.it', password='TestUser1')
        response = self.client.get(reverse('restaurants:orders-managing', args=(0,)))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['restaurateurs_orders'], [])

    def test_orders_managing_with_order_accepted(self):
        """
        orders_managing() shouldn't show the objects that have been accepted
        """
        self.order = Order.objects.create(
            customer=self.customer,
            customer_address=self.customer_address,
            customer_number='3334445556',
            cc_number='123456789',
            restaurant=self.restaurant,
            timestamp=self.now.strftime("%d/%m/%Y, %H:%M"),
            delivery_time=self.now.strftime("%d/%m/%Y, %H:%M"),
            arrival_time=self.now.strftime("%d/%m/%Y, %H:%M"),
            price=10,
            accepted=True,
            done=False,
            deleted_by_customer=False,
            deleted_by_restaurateur=False,
            lock=False,
        )
        self.order.save()

        self.client.login(email='restaurateur@test.it', password='TestUser1')
        response = self.client.get(reverse('restaurants:orders-managing', args=(0,)))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['restaurateurs_orders'], [])

    def test_orders_managing_with_order_deleted_by_restaurateur(self):
        """
        orders_managing() shouldn't show the objects that have been deleted by the restaurateur
        """
        self.order = Order.objects.create(
            customer=self.customer,
            customer_address=self.customer_address,
            customer_number='3334445556',
            cc_number='123456789',
            restaurant=self.restaurant,
            timestamp=self.now.strftime("%d/%m/%Y, %H:%M"),
            delivery_time=self.now.strftime("%d/%m/%Y, %H:%M"),
            arrival_time=self.now.strftime("%d/%m/%Y, %H:%M"),
            price=10,
            accepted=False,
            done=False,
            deleted_by_customer=False,
            deleted_by_restaurateur=True,
            lock=False,
        )
        self.order.save()

        self.client.login(email='restaurateur@test.it', password='TestUser1')
        response = self.client.get(reverse('restaurants:orders-managing', args=(0,)))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['restaurateurs_orders'], [])

    def test_orders_managing_with_order_deleted_by_customer(self):
        """
        orders_managing() should show the objects that have been deleted by the customer
        """
        self.order = Order.objects.create(
            customer=self.customer,
            customer_address=self.customer_address,
            customer_number='3334445556',
            cc_number='123456789',
            restaurant=self.restaurant,
            timestamp=self.now.strftime("%d/%m/%Y, %H:%M"),
            delivery_time=self.now.strftime("%d/%m/%Y, %H:%M"),
            arrival_time=self.now.strftime("%d/%m/%Y, %H:%M"),
            price=10,
            accepted=False,
            done=False,
            deleted_by_customer=True,
            deleted_by_restaurateur=False,
            lock=False,
        )
        self.order.save()

        self.client.login(email='restaurateur@test.it', password='TestUser1')
        response = self.client.get(reverse('restaurants:orders-managing', args=(0,)))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['restaurateurs_orders'], ['<Order: Order object (1)>'])

    def test_orders_managing_with_order(self):
        """
        orders_managing() should show the objects that haven't been accepted nor deleted
        """
        self.order = Order.objects.create(
            customer=self.customer,
            customer_address=self.customer_address,
            customer_number='3334445556',
            cc_number='123456789',
            restaurant=self.restaurant,
            timestamp=self.now.strftime("%d/%m/%Y, %H:%M"),
            delivery_time=self.now.strftime("%d/%m/%Y, %H:%M"),
            arrival_time=self.now.strftime("%d/%m/%Y, %H:%M"),
            price=10,
            accepted=False,
            done=False,
            deleted_by_customer=False,
            deleted_by_restaurateur=False,
            lock=False,
        )
        self.order.save()

        self.client.login(email='restaurateur@test.it', password='TestUser1')
        response = self.client.get(reverse('restaurants:orders-managing', args=(0,)))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['restaurateurs_orders'], ['<Order: Order object (1)>'])

    def test_orders_managing_with_order_lock_true(self):
        """
        orders_managing() should show the objects that haven't been accepted nor deleted
        """
        self.order = Order.objects.create(
            customer=self.customer,
            customer_address=self.customer_address,
            customer_number='3334445556',
            cc_number='123456789',
            restaurant=self.restaurant,
            timestamp=self.now.strftime("%d/%m/%Y, %H:%M"),
            delivery_time=self.now.strftime("%d/%m/%Y, %H:%M"),
            arrival_time=self.now.strftime("%d/%m/%Y, %H:%M"),
            price=10,
            accepted=False,
            done=False,
            deleted_by_customer=False,
            deleted_by_restaurateur=False,
            lock=True,
        )
        self.order.save()

        self.client.login(email='restaurateur@test.it', password='TestUser1')
        response = self.client.get(reverse('restaurants:orders-managing', args=(0,)))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['restaurateurs_orders'], ['<Order: Order object (1)>'])

