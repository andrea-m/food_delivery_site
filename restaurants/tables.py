from django_tables2 import TemplateColumn, tables, Column

from restaurants.models import Order, Restaurant


class RestaurantsTable(tables.Table):

    class Meta:
        template_name = "django_tables2/bootstrap4.html"
        attrs = {"class": "table table-striped table-bordered sortable",
                 "data-toggle": "table"
                 }

    name = Column(verbose_name="Nome")
    add_dish = TemplateColumn(
        exclude_from_export=False,
        template_name='restaurants/django_tables_templates/table_menu_managing.html',
        orderable=False,
        verbose_name='Gestione menu',
        attrs={
            "th": {'style':'text-align: center;'},
            "td": {"align": "center"}
        }
    )
    preview = TemplateColumn(
        exclude_from_export=False,
        template_name='restaurants/django_tables_templates/table_restaurant_preview.html',
        orderable=False,
        verbose_name='Preview',
        attrs={
            "th": {'style':'text-align: center;'},
            "td": {"align": "center"}
        }
    )
    modify = TemplateColumn(
        exclude_from_export=False,
        template_name='restaurants/django_tables_templates/table_modify_restaurant.html',
        orderable=False,
        verbose_name='Modifica ristoranti',
        attrs={
            "th": {'style': 'text-align: center;'},
            "td": {"align": "center"}
        }
    )
    remove = TemplateColumn(
        exclude_from_export=False,
        template_name='restaurants/django_tables_templates/table_restaurant_remove.html',
        orderable=False,
        verbose_name='Rimuovi ristoranti',
        attrs={
            "th": {'style':'text-align: center;'},
            "td": {"align": "center"}
        }
    )


class OrdersTable(tables.Table):

    class Meta:
        template_name = "django_tables2/bootstrap4.html"
        attrs = {"class": "table table-striped table-bordered sortable",
                 "data-toggle": "table"
                 }

    customer = Column(verbose_name='Cliente')
    restaurant = Column(accessor='restaurant.name', verbose_name='Nome del ristorante')
    timestamp = Column(verbose_name='Data ordine')
    price = Column(accessor='get_order_price', verbose_name="Importo dell'ordine", orderable=False)
    details = TemplateColumn(
        exclude_from_export=False,
        template_name='restaurants/django_tables_templates/table_order_details.html',
        orderable=False,
        verbose_name='Dettagli',
        attrs={
            "th": {'style':'text-align: center;'},
            "td": {"align": "center"}
        }
    )
    accepted = TemplateColumn(
        exclude_from_export=False,
        template_name='restaurants/django_tables_templates/table_order_accept.html',
        orderable=False,
        verbose_name='Accetta ordini',
        attrs={
            "th": {'style':'text-align: center;'},
            "td": {"align": "center"}
        }
    )

class AcceptedOrdersTable(tables.Table):
    class Meta:
        template_name = "django_tables2/bootstrap4.html"
        attrs = {"class": "table table-striped table-bordered sortable",
                 "data-toggle": "table"
                 }

    customer = Column(verbose_name='Cliente')
    restaurant = Column(accessor='restaurant.name', verbose_name='Nome del ristorante')
    timestamp = Column(verbose_name='Data ordine')
    price = Column(accessor='get_order_price', verbose_name="Importo dell'ordine", orderable=False)
    details = TemplateColumn(
        exclude_from_export=False,
        template_name='restaurants/django_tables_templates/table_order_details.html',
        orderable=False,
        verbose_name='Dettagli',
        attrs={
            "th": {'style':'text-align: center;'},
            "td": {"align": "center"}
        }
    )
    done = TemplateColumn(
        exclude_from_export=False,
        template_name='restaurants/django_tables_templates/table_order_done.html',
        orderable=False,
        verbose_name='Termina ordini',
        attrs={
            "th": {'style':'text-align: center;'},
            "td": {"align": "center"}
        }
    )

class DoneOrdersTable(tables.Table):

    class Meta:
        template_name = "django_tables2/bootstrap4.html"
        attrs = {"class": "table table-striped table-bordered sortable",
                 "data-toggle": "table"
                 }

    customer = Column(verbose_name='Cliente')
    restaurant = Column(accessor='restaurant.name', verbose_name='Nome del ristorante')
    price = Column(accessor='get_order_price', verbose_name="Importo dell'ordine", orderable=False)
    timestamp = Column(verbose_name='Data ordine')
    details = TemplateColumn(
        exclude_from_export=False,
        template_name='restaurants/django_tables_templates/table_order_details.html',
        orderable=False,
        verbose_name='Dettagli',
        attrs={
            "th": {'style':'text-align: center;'},
            "td": {"align": "center"}
        }
    )
    delete = TemplateColumn(
        exclude_from_export=False,
        template_name='restaurants/django_tables_templates/table_order_remove.html',
        orderable=False,
        verbose_name='Elimina ordini',
        attrs={
            "th": {'style': 'text-align: center;'},
            "td": {"align": "center"}
        }
    )

class MenuTable(tables.Table):

    class Meta:
        template_name = "django_tables2/bootstrap4.html"
        attrs = {"class": "table table-striped table-bordered sortable",
                 "data-toggle": "table"
                 }

    # img = Column(verbose_name='Immagine')
    restaurant = Column(accessor='restaurant.name', verbose_name='Nome del ristorante')
    name = Column(verbose_name='Nome del piatto')
    availability = Column(accessor='get_availability', verbose_name='Disponibilità', orderable=False)
    price = Column(accessor='get_price', verbose_name='Prezzo', orderable=False)
    preparation_time = Column(accessor='get_preparation_time', verbose_name='Tempo di preparazione', orderable=False)
    modify = TemplateColumn(
        exclude_from_export=False,
        template_name='restaurants/django_tables_templates/table_modify_dish.html',
        orderable=False,
        verbose_name='Modifica piatti',
        attrs={
            "th": {'style': 'text-align: center;'},
            "td": {"align": "center"}
        }
    )
    delete = TemplateColumn(
        exclude_from_export=False,
        template_name='restaurants/django_tables_templates/table_dish_remove.html',
        orderable=False,
        verbose_name='Elimina piatti',
        attrs={
            "th": {'style': 'text-align: center;'},
            "td": {"align": "center"}
        }
    )