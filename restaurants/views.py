import datetime
import json

import pytz
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.db import IntegrityError
from django.http import JsonResponse, HttpResponseForbidden
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, FormView
from django_tables2 import RequestConfig

from custom_users.models import Address, CustomUser
from food_delivery import settings
from food_delivery.email_settings import EMAIL_HOST_USER
from restaurants.forms import RestaurantForm, PaymentForm, DishForm
from restaurants.models import Dish, OrderedDish, Review
from restaurants.tables import *


# ------------------ Creation of restaurants -------------------

class RestaurantCreateView(CreateView):
    """
    Handles the creation of new restaurants
    """
    form_class = RestaurantForm
    template_name = 'restaurants/add_restaurant.html'

    def form_valid(self, form):
        restaurateur = self.request.user
        form.instance.restaurateur = restaurateur
        try:
            return super().form_valid(form)
        except IntegrityError:
            messages.error(self.request, 'È già stato inserito un ristorante con questo nome!')
            return self.form_invalid(form)

    def get_success_url(self):
        messages.success(self.request, 'Nuovo ristorante aggiunto.')
        messages.warning(
            self.request,
            'I ristoranti non verranno visualizzati dai clienti fino a quando non vengono aggiunti dei piatti.'
        )
        return reverse("restaurants:restaurants-managing")

# ---------------------------------------------------------------


# ------------------ Creation of dishes -------------------

class DishCreateView(CreateView):
    """
    Handles the creation of new dishes
    """
    form_class = DishForm
    template_name = 'restaurants/add_dish.html'

    def get_context_data(self, **kwargs):
        context = super(DishCreateView, self).get_context_data(**kwargs)
        context['menu_managing_id'] = self.request.session.get('menu_managing_id', 0)
        return context

    def form_valid(self, form):
        form.instance.restaurant = Restaurant.objects.get(id=self.kwargs.get('restaurant_id', None))
        try:
            return super().form_valid(form)
        except IntegrityError:
            messages.error(self.request, 'È già stato inserito un piatto con questo nome!')
            return self.form_invalid(form)

    def get_success_url(self):
        messages.success(self.request, 'Nuovo piatto aggiunto.')
        return reverse("restaurants:menu-managing", args=(self.kwargs.get('restaurant_id', None),))

# ----------------------------------------------------------


# ------------------ Restaurants management -------------------

def get_max_delivery_time(restaurant, travel_time):
    """
    Returns the delivery time calculated as the maximum preparation time for the dishes plus the travel time
    from the restaurant to the customer's house
    :param restaurant: restaurant
    :param travel_time: travel time from the restaurant to the customer's house
    :return: delivery time in minutes
    """
    restaurant_dishes = Dish.objects.filter(restaurant=restaurant).filter(availability=True)
    max_preparation_time = 0
    for dish in restaurant_dishes:
        if dish.preparation_time > max_preparation_time:
            max_preparation_time = dish.preparation_time
    return max_preparation_time + travel_time


def restaurants(request, pk):
    """
    Shows the restaurant page with his data, the distance and travel time from the customer, the average rating,
    the dishes, the shopping cart, the rating input system (that can only be accessed by authenticated customers that
    made an order to the restaurant) and all the past ratings (that can only be accessed by authenticated customers)
    """
    restaurant = Restaurant.objects.get(id=pk)
    request.session['restaurant_id'] = restaurant.id
    dishes = Dish.objects.filter(restaurant=pk)
    user = request.user
    user_authenticated = False
    if user.is_authenticated:
        user_authenticated = True
    user_address = request.session.get('input_address', None)

    # Delivery info
    distance_duration = request.session.get('distance_duration'+pk.__str__(), None)
    if request.user.is_authenticated and request.user.is_restaurateur:
        delivery_time = '*Tempi di spedizione*'
        distance = '*Distanza*'
    else:
        distance = round(distance_duration.get('distance', None), 1)
        travel_time = distance_duration.get('duration', None)
        request.session['travel_time'] = travel_time
        delivery_time = int(get_max_delivery_time(restaurant, travel_time))
        # request.session['delivery_time'] = delivery_time

    # Cart info
    cart_elements_dict = request.session.get('cart_elements', None)
    cart_elements = list()
    cart_restaurant = ''
    if cart_elements_dict != None:
        cart_elements = cart_elements_dict.get('elements', {})
        cart_restaurant = int(cart_elements_dict.get('restaurant', '0'))

    # Review info
    user_made_order = False
    user_reviewed_restaurant = False
    user_review = []
    user = request.user
    if user.is_authenticated:
        order = Order.objects.filter(restaurant=restaurant).filter(customer=user)
        if len(order) != 0:
            user_made_order = True
        user_reviews = Review.objects.filter(reviewer=user)
        if len(user_reviews) != 0:
            user_review = user_reviews.filter(restaurant=restaurant)
        if len(user_review) != 0:
            user_reviewed_restaurant = True
            user_review = user_reviews.get(restaurant=restaurant)
        user_name = user.first_name
    else:
        user_name = ''
    reviews = Review.objects.filter(restaurant=restaurant.id)

    # Rating info
    ratings_total = 0
    reviews_number = len(reviews)
    for rating in reviews.values_list('rating', flat=True):
        ratings_total += rating
    if reviews_number != 0:
        average_rating = round(ratings_total/len(reviews), 1)
    else:
        average_rating = 0
    if user_authenticated and user_reviewed_restaurant:
        user_rating = reviews.get(reviewer=user).rating
    else:
        user_rating = None

    cart_total = request.session.get('cart_elements', {}).get('total', 0.0)

    context = {
        'restaurant': restaurant, 'dishes': dishes, 'media_url': settings.MEDIA_URL, 'user_address': user_address,
        'delivery_time': delivery_time, 'cart_restaurant': cart_restaurant, 'cart_elements': cart_elements,
        'cart_elements_number': len(cart_elements), 'user_made_order': user_made_order,
        'user_reviewed_restaurant': user_reviewed_restaurant, 'user_review': user_review,
        'user_authenticated': user_authenticated, 'reviews': reviews, 'stars_number': [1,2,3,4,5],
        'average_rating': average_rating, 'reviews_number': reviews_number, 'distance': distance,
        'cart_total': cart_total, 'user_name': user_name, 'user_rating': user_rating
    }

    return render(request, 'restaurants/restaurant_page.html', context)


def modify_restaurant(request, id=None, template_name='restaurants/modify_restaurant.html'):
    """
    Handles the modification of a restaurant
    """
    if id:
        restaurant = get_object_or_404(Restaurant, pk=id)
        if restaurant.restaurateur != request.user:
            return HttpResponseForbidden()
    else:
        return

    form = RestaurantForm(request.POST or None, instance=restaurant)
    if request.POST and form.is_valid():
        form.save()

        # Save was successful, so redirect to another page
        messages.success(request, 'Ristorante modificato.')
        redirect_url = reverse('restaurants:restaurants-managing')
        return redirect(redirect_url)

    name = restaurant.name
    return render(request, template_name, {
        'form': form,
        'name': name,
    })


def remove_restaurant(request, id):
    """
    Handles the removal of a restaurant
    """
    Restaurant.objects.get(id=id).delete()
    messages.success(request, 'Ristorante rimosso.')
    return redirect('restaurants:restaurants-managing')


def restaurants_managing(request):
    """
    Shows the restaurateur all the restaurants that he added to the platform and gives him the opportunity to modify
    them, delete them or add new ones.
    """
    restaurateurs_restaurants = Restaurant.objects.filter(restaurateur=request.user)
    restaurants_number = len(restaurateurs_restaurants)
    table = RestaurantsTable(restaurateurs_restaurants)
    RequestConfig(request).configure(table)
    context = {'table': table, 'user_name': request.user.first_name, 'restaurants_number': restaurants_number}
    return render(request, 'restaurants/restaurants_managing.html', context)

# ---------------------------------------------------------


# ------------------ Menu management -------------------

def menu_managing(request, id=0):
    """
    Shows the restaurateur all the dishes that he added to the restaurants and gives him the opportunity to modify
    them, delete them or add new ones.
    """
    request.session['menu_managing_id'] = id
    restaurateurs_restaurants = Restaurant.objects.filter(restaurateur=request.user)
    if id.__str__() == '0':
        restaurateurs_menu = Dish.objects.filter(restaurant=restaurateurs_restaurants[0])
        restaurant = restaurateurs_restaurants[0]
    else:
        restaurateurs_menu = Dish.objects.filter(restaurant=id.__str__())
        restaurant = Restaurant.objects.get(id=id)
    multiple_restaurants = False
    if len(restaurateurs_restaurants) > 1:
        multiple_restaurants = True
    dishes_number = len(restaurateurs_menu)
    table = MenuTable(restaurateurs_menu)
    context = {'table': table, 'user_name': request.user.first_name, 'multiple_restaurants': multiple_restaurants,
               'restaurateurs_restaurants': restaurateurs_restaurants, 'dishes_number': dishes_number,
               'restaurant': restaurant}
    RequestConfig(request).configure(table)
    return render(request, 'restaurants/menu_managing.html', context)


def modify_dish(request, id=None, template_name='restaurants/modify_dish.html'):
    """
    Handles the dishes' modification
    """
    if id:
        dish = get_object_or_404(Dish, pk=id)
        if dish.restaurant.restaurateur != request.user:
            return HttpResponseForbidden()
    else:
        return

    form = DishForm(request.POST or None, instance=dish)
    if request.POST and form.is_valid():
        form.save()

        # Save was successful, so redirect to another page
        messages.success(request, 'Piatto modificato.')
        redirect_url = reverse('restaurants:menu-managing', args=(dish.restaurant.id,))
        return redirect(redirect_url)

    name = dish.name
    menu_managing_id = request.session.get('menu_managing_id', 0)
    return render(request, template_name, {
        'form': form,
        'name': name,
        'menu_managing_id': menu_managing_id,
    })


def remove_dish(request, id):
    """
    Handles the dishes' removal
    """
    Dish.objects.get(id=id).delete()
    messages.success(request, 'Piatto eliminato.')
    return redirect(request.META.get('HTTP_REFERER'))

# ---------------------------------------------------------


# ------------------ Orders management -------------------

def orders_managing(request, restaurant_id_order=0):
    """
    Shows the restaurateur all the orders that are not accepted nor deleted nor done and gives him the opportunity to
    see the details (all the orders' data) or to accept them
    """
    request.session['order_managing_now'] = {'type': 'base', 'id': restaurant_id_order}
    restaurateurs_restaurants = Restaurant.objects.filter(restaurateur=request.user)
    if restaurant_id_order.__str__() == '0':
        restaurateurs_orders = Order.objects.filter(restaurant=restaurateurs_restaurants[0]) \
            .filter(accepted=False) \
            .filter(deleted_by_restaurateur=False)
        restaurant = Restaurant.objects.get(id=restaurateurs_restaurants[0].id)
    else:
        restaurateurs_orders = Order.objects.filter(restaurant_id=restaurant_id_order.__str__()) \
            .filter(accepted=False) \
            .filter(deleted_by_restaurateur=False)
        restaurant = Restaurant.objects.get(id=restaurant_id_order.__str__())
    multiple_restaurants = False
    if len(restaurateurs_restaurants) > 1:
        multiple_restaurants = True
    orders_number = len(restaurateurs_orders)
    table = OrdersTable(restaurateurs_orders)
    context = {'table': table, 'user_name': request.user.first_name, 'multiple_restaurants': multiple_restaurants,
               'restaurateurs_restaurants': restaurateurs_restaurants, 'orders_number': orders_number,
               'restaurateurs_orders': restaurateurs_orders, 'restaurant': restaurant}
    RequestConfig(request).configure(table)
    return render(request, 'restaurants/orders_managing.html', context)


def order_accept(request, id):
    """
    Handles the acceptance of the orders
    """
    order = Order.objects.get(id=id)
    if order.lock:
        messages.error(request, "Si è verificato un accesso simultaneo all'ordine selezionato. "
                                "Non sarà possibile accettarlo finché non verrà liberato!")
        return redirect(request.META.get('HTTP_REFERER'))
    else:
        order.lock = True
        order.save()

    # Arrival time calculation
    tz = pytz.timezone('Europe/Berlin')
    now = datetime.datetime.now(tz)
    arrival_time = (now + datetime.timedelta(minutes=int(order.delivery_time)))
    arrival_time = arrival_time.strftime("%d/%m/%Y, %H:%M")
    order.arrival_time = arrival_time

    # Sends a confirmation email to the customer
    cart_elements = OrderedDish.objects.filter(order=order)
    cart_restaurant = order.restaurant.name
    delivery_price = order.price
    arrival_time = arrival_time[-5:]
    message = "Il suo ordine effettuato presso il ristorante " + cart_restaurant.__str__() + " e comprendente:\n\n"
    for element in cart_elements:
        quantity = element.quantity
        name = element.dish.name
        message += '- ' + quantity.__str__() + \
                   ' ' + name.__str__() + ',\n'
    message += "\nper un totale di € " + delivery_price.__str__() + ", compresa spedizione, "
    message += "è stato preso in carico dal ristorante.\n" \
               "L'arrivo è stimato per le ore:\n"
    message += arrival_time.__str__() + '\n'
    send_mail(
        "Conferma ordine",
        message,
        EMAIL_HOST_USER,
        [order.customer.email],
        fail_silently=False,
    )

    order.accepted = True
    order.lock = False
    order.save()
    messages.success(request, 'Ordine accettato')
    return redirect(request.META.get('HTTP_REFERER'))


def order_done(request, id):
    """
    Handles the marking as done of the orders
    """
    order = Order.objects.get(id=id)
    order.done=True
    order.save()
    messages.success(request, 'Ordine terminato.')
    return redirect(request.META.get('HTTP_REFERER'))


def accepted_orders_managing(request, restaurant_id_order=0):
    """
    Shows the restaurateur all the orders that have been accepted and that haven't been deleted nor done and gives him
    the opportunity to see the details or mark them as done
    """
    request.session['order_managing_now'] = {'type': 'accepted', 'id': restaurant_id_order}
    restaurateurs_restaurants = Restaurant.objects.filter(restaurateur=request.user)
    if restaurant_id_order.__str__() == '0':
        restaurateurs_orders = Order.objects.filter(restaurant=restaurateurs_restaurants[0]) \
            .filter(accepted=True) \
            .filter(done=False) \
            .filter(deleted_by_restaurateur=False)
        restaurant = Restaurant.objects.get(id=restaurateurs_restaurants[0].id)
    else:
        restaurateurs_orders = Order.objects.filter(restaurant_id=restaurant_id_order.__str__()) \
            .filter(accepted=True) \
            .filter(done=False) \
            .filter(deleted_by_restaurateur=False)
        restaurant = Restaurant.objects.get(id=restaurant_id_order.__str__())
    multiple_restaurants = False
    if len(restaurateurs_restaurants) > 1:
        multiple_restaurants = True
    orders_number = len(restaurateurs_orders)
    table = AcceptedOrdersTable(restaurateurs_orders)
    RequestConfig(request).configure(table)
    context = {'table': table, 'user_name': request.user.first_name, 'multiple_restaurants': multiple_restaurants,
               'restaurateurs_restaurants': restaurateurs_restaurants, 'orders_number': orders_number,
               'restaurant': restaurant}
    return render(request, 'restaurants/accepted_orders_managing.html', context)


def done_orders_managing(request, restaurant_id_order=0):
    """
    Shows the restaurateur all the orders that have been accepted, marked as done and that haven't been deleted
    and gives him the opportunity to see the details or delete them
    """
    request.session['order_managing_now'] = {'type': 'done', 'id': restaurant_id_order}
    restaurateurs_restaurants = Restaurant.objects.filter(restaurateur=request.user)
    if restaurant_id_order.__str__() == '0':
        restaurateurs_orders = Order.objects.filter(restaurant=restaurateurs_restaurants[0]) \
            .filter(done=True) \
            .filter(deleted_by_restaurateur=False)
        restaurant = Restaurant.objects.get(id=restaurateurs_restaurants[0].id)
    else:
        restaurateurs_orders = Order.objects.filter(restaurant_id=restaurant_id_order.__str__()) \
            .filter(done=True) \
            .filter(deleted_by_restaurateur=False)
        restaurant = Restaurant.objects.get(id=restaurant_id_order.__str__())
    multiple_restaurants = False
    if len(restaurateurs_restaurants) > 1:
        multiple_restaurants = True
    orders_number = len(restaurateurs_orders)
    table = DoneOrdersTable(restaurateurs_orders)
    RequestConfig(request).configure(table)
    context = {'table': table, 'user_name': request.user.first_name, 'multiple_restaurants': multiple_restaurants,
               'restaurateurs_restaurants': restaurateurs_restaurants, 'orders_number': orders_number,
               'restaurant': restaurant}
    return render(request, 'restaurants/done_orders_managing.html', context)


def order_details(request, id):
    """
    Shows all the order's data
    """
    order = Order.objects.get(id=id)
    ordered_dishes_ref = OrderedDish.objects.filter(order=order)
    ordered_dishes = list()
    for ordered_dish in ordered_dishes_ref:
        dish = Dish.objects.get(id=ordered_dish.dish.id)
        dishes_dict = dict()
        dishes_dict['name'] = dish.name
        dishes_dict['quantity'] = ordered_dish.quantity
        dishes_dict['price'] = dish.price * ordered_dish.quantity
        ordered_dishes.append(dishes_dict)
    order_managing_now = request.session.get('order_managing_now')
    order_managing_type = order_managing_now.get('type', 'base')
    order_managing_id = order_managing_now.get('id', 0)
    context = {'order': order, 'ordered_dishes': ordered_dishes, 'order_managing_id': order_managing_id,
               'order_managing_type': order_managing_type}
    return render(request, 'restaurants/order_details.html', context)


def remove_order(request, id):
    """
    Handles the order's removal
    """
    order = Order.objects.get(id=id)
    order.deleted_by_restaurateur = True
    order.save()
    messages.success(request, 'Ordine eliminato.')
    return redirect(request.META.get('HTTP_REFERER'))

# ---------------------------------------------------------


@method_decorator(login_required, name='dispatch')
class Payment(FormView):
    """
    Handles the order payment and creates the Order object with the correlated OrderedDish ones
    """
    form_class = PaymentForm
    template_name = 'restaurants/payment.html'
    success_url = reverse_lazy('home')

    cart_elements_dict  = None
    cart_elements       = None
    cart_restaurant     = None
    delivery_price      = None
    cart_total          = None

    def get_cart_total(self, cart_elements, delivery_price):
        total = float(delivery_price)
        for element in cart_elements:
            total += int(element.get('dish_quantity', 0)) * float(element.get('price', 0))
        return total

    def get_context_data(self, **kwargs):
        context = super(Payment, self).get_context_data(**kwargs)
        self.cart_elements_dict = self.request.session.get('cart_elements', None)
        self.cart_elements = self.cart_elements_dict.get('elements', None)
        self.cart_restaurant = self.cart_elements_dict.get('restaurant', None)
        self.delivery_price = Restaurant.objects.get(id=self.cart_restaurant).delivery_price
        self.cart_total = self.get_cart_total(self.cart_elements, self.delivery_price)
        if self.request.user.is_authenticated:
            user_name = self.request.user.first_name
        else:
            user_name = ''
        context['cart_elements'] = self.cart_elements
        context['cart_total'] = self.cart_total
        context['delivery_price'] = self.delivery_price
        context['user_name'] = user_name
        context['restaurant_id'] = int(self.request.session.get('cart_elements', {})
                                        .get('restaurant', '0'))
        return context

    def form_valid(self, form):
        self.cart_elements_dict = self.request.session.get('cart_elements', None)
        self.cart_elements = self.cart_elements_dict.get('elements', None)
        self.cart_restaurant = self.cart_elements_dict.get('restaurant', None)
        self.delivery_price = Restaurant.objects.get(id=self.cart_restaurant).delivery_price
        self.cart_total = self.get_cart_total(self.cart_elements, self.delivery_price)
        user_address = self.request.session.get('input_address', None)

        # Creates the Address object
        customer_address=Address.objects.create(
            user=self.request.user,
            street=user_address.get('street', ''),
            house_number=user_address.get('house_number', ''),
            city=user_address.get('city', ''),
            province=user_address.get('province', ''),
            zipcode=user_address.get('zipcode', ''),
        )

        # Gets current date and time
        tz = pytz.timezone('Europe/Berlin')
        now = datetime.datetime.now(tz)

        # Creates the Order object
        restaurant = Restaurant.objects.get(id=self.cart_restaurant)
        order = Order.objects.create(
            customer=self.request.user,
            customer_address=customer_address,
            customer_floor_flat_nb=form.cleaned_data.get('customer_floor_flat_nb'),
            customer_number=form.cleaned_data.get('customer_number'),
            instructions=form.cleaned_data.get('instructions'),
            cc_number=form.cleaned_data.get('cc_number'),
            restaurant=restaurant,
            timestamp=now.strftime("%d/%m/%Y, %H:%M"),
            delivery_time=self.request.session.get('delivery_time'),
            arrival_time="",
            price=self.cart_total,
            done=False,
        )

        # Sends a confirmation email to the customer
        message = "Il suo ordine effettuato presso il ristorante " + restaurant.name.__str__() + " e comprendente:\n\n"
        for element in self.cart_elements:
            quantity = element.get('dish_quantity', '0')
            name = element.get('dish_name', '')
            price = element.get('price', '0')
            message += '- ' + quantity.__str__() + \
                       ' ' + name.__str__() + ',\n'
        message += "\nper un totale di € " + self.cart_total.__str__() + ", compresa spedizione, "
        message += "è stato ricevuto correttamente.\n" \
                   "Seguirà una nuova mail di conferma non appena il ristoratore avrà accettato l'ordine."
        send_mail(
            "Ordine ricevuto",
            message,
            EMAIL_HOST_USER,
            [self.request.user.email],
            fail_silently=False,
        )

        # Sends an email to the restaurateur to notify the new order
        restaurateur_email = CustomUser.objects.get(email=Restaurant.objects.get(id=self.cart_restaurant).restaurateur).email
        customer = self.request.user
        message = 'Nuovo ordine da ' + customer.first_name + ', email: ' + customer.email +'.\n' + \
                  "L'ordine comprende: \n"
        total = 0.0
        for element in self.cart_elements:
            quantity = element.get('dish_quantity', '0')
            name = element.get('dish_name', '')
            price = element.get('price', '0')
            message += '- ' + quantity.__str__() + \
                       ' ' + name.__str__() + ',\n'
            total += int(quantity) * float(price)
        total += float(self.delivery_price)
        message += "per un totale di € " + total.__str__() + " compresa spedizione.\n"
        send_mail(
            "Ordine n°"+order.id.__str__(),
            message,
            EMAIL_HOST_USER,
            [restaurateur_email],
            fail_silently=False,
        )

        for element in self.cart_elements:
            dish = Dish.objects.filter(restaurant=restaurant).get(name=element.get('dish_name',''))
            OrderedDish.objects.create(
                order=order,
                dish=dish,
                quantity=element.get('dish_quantity', 0)
            )
        self.request.session['cart_elements'] = {}
        messages.success(self.request, 'Ordine effettuato.')
        return redirect('show_restaurants_in_range')


def get_delivery_time(restaurant_object, cart_elements, travel_time):
    """
    Returns the delivery time calculated as the maximum preparation time for the dishes in the cart (assuming they
    can be prepared simultaneously) plus the travel time from the restaurant to the customer's house
    :param restaurant: restaurant
    :param travel_time: travel time from the restaurant to the customer's house
    :return: delivery time in minutes
    """
    restaurant_dishes = Dish.objects.filter(restaurant=restaurant_object)
    max_preparation_time = 0
    for element in cart_elements:
        dish = restaurant_dishes.get(name=element.get('dish_name'))
        if dish.preparation_time > max_preparation_time:
            max_preparation_time = dish.preparation_time
    return max_preparation_time + travel_time


def ajax_get_cart_data(request):
    """
    Gets the cart data from the Ajax function in restaurant_page.html, checks if the data is correct comparing it to
    the one stored in the database and if it is it stores it in a session variable
    """
    restaurant = request.POST.get('restaurant', '')
    restaurant_object = Restaurant.objects.get(id=restaurant)
    cart_elements_str = request.POST.get('elements', {})
    cart_elements = json.loads(cart_elements_str)
    request.session['cart_elements'] = {
        'restaurant'    : restaurant,
        'elements' : None,
    }

    # Ajax data check
    correct = True
    for element in cart_elements:
        dishes = Dish.objects.filter(restaurant=restaurant)
        # Checks if the name of the dish is present in the db
        if len(dishes.filter(name=element['dish_name'])) == 0:
            correct = False
            break
        dish = Dish.objects.filter(name=element['dish_name']).get(restaurant=restaurant)
        # Checks if the dish is available
        if not dish.availability:
            correct = False
            break
        # Checks if the price corresponds
        if not dish.price == element['price']:
            correct = False
            break
    if correct == True:
        request.session['cart_elements']['elements'] = cart_elements
        travel_time = request.session.get('travel_time', 0)
        request.session['delivery_time'] = int(get_delivery_time(restaurant_object, cart_elements, travel_time))

        # Cart total calculation
        cart_total = 0.0
        if len(cart_elements) != 0:
            for element in cart_elements:
                cart_total += int(element.get('dish_quantity')) * float(element.get('price'))
        else:
            cart_total = 0.0
        if cart_total > 0:
            request.session['cart_elements']['total'] = cart_total + float(restaurant_object.delivery_price)
        else:
            request.session['cart_elements']['total'] = 0.0

    # redirect = reverse('restaurants:payment')
    data = {
        'correct':  correct,
        # 'redirect': redirect
    }
    return JsonResponse(data)


def ajax_get_rating_data(request):
    """
    Gets the rating data from the Ajax function in restaurant_page.html, checks if the data is correct and if it is
    it stores it creates the new Review object
    """
    rating = int(request.POST.get('rating', -1))
    rating_comment = request.POST.get('rating_comment', '')
    modification = json.loads(request.POST.get('modification', 'false'))
    restaurant = Restaurant.objects.get(id=request.session.get('restaurant_id', -1))
    # if modification=True the user is modifying his review so the old one needs to be deleted
    if modification == True:
        Review.objects.filter(reviewer=request.user).get(restaurant=restaurant).delete()
    data = {
        'error': False,
    }

    # Checks if the rating is in the 1-5 range
    if not rating >= 0 and rating <= 5:
        data['error'] = True
    else:
        Review.objects.create(
            restaurant=restaurant,
            reviewer=request.user,
            reviewer_name=request.user.first_name,
            rating=rating,
            comment=rating_comment,
        )
    return JsonResponse(data)
