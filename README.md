# Food Delivery
Sito per la gestione della consegna a domicilio di cibo da parte di ristoranti.

Clone the repo in you directory

## Prerequisiti
* [python 3.5 o superiore](https://realpython.com/installing-python/)
* pipenv
  <br>
  installazione:
  ```
  $ pip3 install pipenv
  ```

  
## Librerie
* django
* pillow
* django-braces
* django-crispy-forms
* django-tables2
* upenrouteservice
* pytz
* requests
* sqlparse
* chardet

Per installare le librerie posizionarsi nella cartella del progetto (food_delivery_site) ed eseguire da terminale:
```
$ pipenv install
```

## Configurazione email
All'interno di food_delivery_site/food_delivery inserire un file con nome 'email_settings.py' e inserirvi i seguenti parametri:
```
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587  # SSL
EMAIL_USE_TLS = True
SERVER_EMAIL = DEFAULT_FROM_EMAIL = EMAIL_HOST_USER = 'il_tuo_indirizzo_mail'
EMAIL_HOST_PASSWORD = 'la_tua_password'
```
sostituendo il_tuo_indirizzo_mail e la_tua_password con un effettivo indirizzo email e relativa password.

## Esecuzione
Posizionarsi nella cartella del progetto (food_delivery_site) ed eseguire da terminale:
```
$ pipenv shell
```
per attivare il virtual environment e
```
$ python manage.py runserver
```
per lanciare il web server.
<br>
Infine cliccare sul link restituito.

## Esecuzione test
Posizionarsi nella cartella del progetto (food_delivery_site) ed eseguire da terminale:
```
$ python manage.py test
```