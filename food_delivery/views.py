from django.contrib import messages
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import FormView

from custom_users.models import CustomUser
from food_delivery import settings
from food_delivery.forms import InputAddressForm
from food_delivery.maps_services import MapsServices
from restaurants.models import Restaurant, Dish


def home(request):
    """
    Renders the generic homepage if the user is not logged in, otherwise redirects
    the user to the relative homepage.
    """
    cart_total = request.session.get('cart_elements', {}).get('total', 0.0)
    context = {'cart_total': cart_total}

    # Sets the distance range in wich the restaurants can deliver to customers
    request.session['delivery_range'] = 40

    if request.user.is_authenticated:
        user_email = request.user.email
        user = CustomUser.objects.get(email=user_email)
        if user.is_customer:
            return redirect('custom_users:customer-home')
        elif user.is_restaurateur:
            return redirect('restaurants:restaurants-managing')
        else:
            return render(request, 'home.html', context)
    else:
        return render(request, 'home.html', context)


def show_restaurants_in_range(request):
    """
    Gets the address entered by the user and using the maps API calculates the distance between it and the
    restaurants one. Then it shows the user the restaurants tha are within the range established in the home view.
    """
    customer_street = request.session['customer_street']
    restaurants_in_range_ids = request.session['restaurants_in_range_ids']
    restaurants_in_range = Restaurant.objects.filter(pk__in=restaurants_in_range_ids)
    cart_total = request.session.get('cart_elements', {}).get('total', 0.0)
    if request.user.is_authenticated:
        user_name = request.user.first_name
    else:
        user_name = ''
    context = {'restaurants_in_range': restaurants_in_range, 'customer_street': customer_street, 'cart_total': cart_total,
               'user_name': user_name, 'media_url': settings.MEDIA_URL,}
    return render(request, 'show_restaurants_in_range.html', context)


def get_restaurants_in_range(street, house_number, city, province, range, input_address):
    """
    Gets the address entered by the user, gets the distance between it and all the restaurant's addresses using the
    maps API and checks if the restaurants are in the distance range established in the home view.
    :param street: street entered by the user
    :param house_number: house number entered by the user
    :param city: city entered by the user
    :param province: province entered by the user
    :param range: range in which the restaurants can ship
    :param input_address: InputAddress object used to set a session variable
    :return: objects representing the restaurants that are in range
    """
    maps_services = MapsServices()
    restaurants_in_range = list()
    for restaurant in Restaurant.objects.all():
        customer_coordinates = maps_services.get_coordinates(
            street,
            house_number,
            city,
            province
        )

        restaurant_street = restaurant.street
        restaurant_house_number = restaurant.house_number
        restaurant_city = restaurant.city
        restaurant_province = restaurant.province
        restaurant_coordinates = maps_services.get_coordinates(
            restaurant_street,
            restaurant_house_number,
            restaurant_city,
            restaurant_province
        )
        if customer_coordinates == None:
            return restaurants_in_range
        distance_duration = maps_services.get_distance_and_duration(customer_coordinates, restaurant_coordinates)
        input_address.request.session['distance_duration'+restaurant.id.__str__()] = distance_duration
        # print("Distanza: " + distance.__str__())
        if distance_duration.get('distance', None) < range:
            # Also check if restaurants have come associated dishes
            dishes = Dish.objects.filter(restaurant=restaurant)
            # If the restaurant doesn't have dishes it's not shown to the customer
            if len(dishes) > 0:
                restaurants_in_range.append(restaurant.id)
            print("YES\n\n")
        else:
            pass
            print("NO\n\n")
    return restaurants_in_range


class InputAddress(FormView):
    """
    Gets the address entered by the user
    """
    form_class = InputAddressForm
    template_name = 'input_address.html'
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        if form.is_valid():
            street = form.cleaned_data['street']
            house_number = form.cleaned_data['house_number']
            city = form.cleaned_data['city']
            province = form.cleaned_data['province']
            customer_street = street
            self.request.session['input_address'] = form.cleaned_data
            self.request.session['customer_street'] = customer_street
            range = self.request.session.get('delivery_range', 1000000)
            restaurants_in_range_ids = get_restaurants_in_range(street, house_number, city, province, range, self)
            # print(type(restaurants_in_range_ids[0]))
            self.request.session['restaurants_in_range_ids'] = restaurants_in_range_ids
            print('restaurants_in_range_ids: ', restaurants_in_range_ids)
        if len(restaurants_in_range_ids) == 0:
            messages.error(self.request, "Non sono stati trovati ristoranti in grado di spedire all'indirizzo indicato. "
                                         "Controllare i campi o immettere un nuovo indirizzo e riprovare.")
            return redirect('input-address')
        return redirect('show_restaurants_in_range')
