from django.test import TestCase
from food_delivery.maps_services import *

class MapsServicesTests(TestCase):
    def test_get_coordinates_no_sense_data(self):
        """
        If the input data doesn't make sense get_coordinates() shouldn't be able
        to retrieve the information about the coordinates and should return None
        """
        maps_services = MapsServices()
        result = maps_services.get_coordinates(
            '123',
            'qwerty',
            '456',
            'asdf'
        )
        self.assertEqual(result, None)

    def test_get_coordinates_sensible_data(self):
        """
        If the input data is sensible get_coordinates() should be able
        to retrieve the information about the coordinates and should not return None
        """
        maps_services = MapsServices()
        result = maps_services.get_coordinates(
            'Piazza del Colosseo',
            '1',
            'Roma',
            'RM'
        )
        self.assertNotEqual(result, None)


    def test_get_distance_and_duration_uncorrect_type_data_str(self):
        """
        If the input data is of incorrect type get_distance_and_duration() should return None
        """
        maps_services = MapsServices()
        result = maps_services.get_distance_and_duration(
            'asdf',
            'qwert',
        )
        self.assertEqual(result, None)

    def test_get_distance_and_duration_uncorrect_type_data_int(self):
        """
        If the input data is of incorrect type get_distance_and_duration() should return None
        """
        maps_services = MapsServices()
        result = maps_services.get_distance_and_duration(
            123,
            456,
        )
        self.assertEqual(result, None)

    def test_get_distance_and_duration_uncorrect_type_data_float(self):
        """
        If the input data is of incorrect type get_distance_and_duration() should return None
        """
        maps_services = MapsServices()
        result = maps_services.get_distance_and_duration(
            123.1,
            456.2,
        )
        self.assertEqual(result, None)

    def test_get_distance_and_duration_uncorrect_type_data_dict(self):
        """
        If the input data is of incorrect type get_distance_and_duration() should return None
        """
        maps_services = MapsServices()
        result = maps_services.get_distance_and_duration(
            {'a': 123.1},
            {'b': 456.2},
        )
        self.assertEqual(result, None)

    def test_get_distance_and_duration_uncorrect_type_data_list_str(self):
        """
        If the input data is of incorrect type get_distance_and_duration() should return None
        """
        maps_services = MapsServices()
        result = maps_services.get_distance_and_duration(
            ['abc', 'def'],
            ['ghi', 'jqk'],
        )
        self.assertEqual(result, None)

    def test_get_distance_and_duration_no_sense_data(self):
        """
        If the input data is of correct type but the values doesn't make sense or the API is not able to retrieve the
        information and get_distance_and_duration() should return two 1000000 values representing infinity
        """
        maps_services = MapsServices()
        # I'll give coordinates out of range
        result = maps_services.get_distance_and_duration(
            [200.0, 200.0],
            [300.0, 300.0],
        )
        self.assertGreaterEqual(result.get('distance'), 1000000)
        self.assertGreaterEqual(result.get('duration'), 1000000)

    def test_get_distance_and_duration_sensible_data(self):
        """
        If the input data is of correct type and the values make sense, the API should be able to retrieve the
        information and get_distance_and_duration() should return sensible values
        """
        maps_services = MapsServices()
        Colosseo_coordinates = maps_services.get_coordinates(
            'Piazza del Colosseo',
            '1',
            'Roma',
            'RM'
        )
        Pantheon_coordinates = maps_services.get_coordinates(
            'Piazza della Rotonda',
            'Roma',
            'RM'
        )
        result = maps_services.get_distance_and_duration(
            Colosseo_coordinates,
            Pantheon_coordinates,
        )
        self.assertGreaterEqual(result.get('distance'), 3)
        self.assertGreaterEqual(result.get('duration'), 10)
