from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms

from custom_users.models import Address


class InputAddressForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(
            Submit('submit', 'Cerca', css_class='btn btn-success')
        )

    class Meta:
        model = Address
        fields = (
            'street',
            'house_number',
            'city',
            'province',
            'zipcode',
        )
        labels = {
            'street':           'Via',
            'house_number':     'Numero civico',
            'city':             'Città',
            'province':         'Provincia',
            'zipcode':          'Cap',
        }