from django.contrib import admin
from django.urls import path, include
from food_delivery import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.home, name='home'),
    path('input_address/', views.InputAddress.as_view(), name='input-address'),
    path('restaurants_in_range/', views.show_restaurants_in_range, name='show_restaurants_in_range'),
    path('admin/', admin.site.urls),
    path('auth_user/', include('custom_users.urls')),
    path('restaurants/', include('restaurants.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
