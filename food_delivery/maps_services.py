import requests
import json


class MapsServices:
    """
    Class that uses the openrouteservice's api to retrieve informations about the distance and the travel time
    between two places
    """
    request_text = 'https://api.openrouteservice.org/geocode/search?api_key=5b3ce3597851110001cf62488324b4840be643babfd1a938709f2a57&text='

    def get_coordinates(self, *args):
        """
        Returns the coordinates corresponding to the place given in input
        :param args: Names of the place
        :return: list containing the coordinates
        """
        request_text = self.request_text
        for arg in args:
            if not isinstance(arg, str):
                return
            request_text += str(arg) + '%20'
        request_text = request_text[:len(request_text) - 3]
        headers = {
            'Accept': 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8',
        }
        call = requests.get(request_text, headers=headers)
        parsed_json = (json.loads(call.text))
        parsed_json_features = parsed_json.get('features', '')
        if parsed_json_features == '' or parsed_json_features == []:
            return None
        coordinates = parsed_json_features[0].get('geometry', '').get('coordinates', '')
        return coordinates

    def get_distance_and_duration(self, A, B):
        """
        Returns the distance in Km and the duration in minutes of the road route from A to B
        :param A: coordinates of place A
        :param B: coordinates of place B
        :return: dictionary containing the distance in Km and the duration in minutes
        """
        if not isinstance(A, list) or not isinstance(B, list):
            return
        if not len(A) == 2 and not len(B) == 2:
            return
        for a, b in (A, B):
            if not isinstance(a, float) or not isinstance(b, float):
                return

        headers = {
            'Accept': 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8',
        }
        call = requests.get(
            'https://api.openrouteservice.org/v2/directions/driving-car?'
            'api_key=5b3ce3597851110001cf62488324b4840be643babfd1a938709f2a57&'
            'start={},%20{}&'
            'end={},%20{}'.format(A[0], A[1], B[0], B[1]),
            headers=headers)
        parsed_json = (json.loads(call.text))
        distance_duration = dict()
        parsed_json_features = parsed_json.get('features', '')

        # If the values are not found a big value is returned
        if parsed_json_features == '':
            distance_duration['distance'] = 1000000
            distance_duration['duration'] = 1000000
            return distance_duration
        distance_duration_values = parsed_json_features[0].get('properties', '').get('summary', '')
        distance_duration['distance'] = distance_duration_values.get('distance', 1000000000) / 1000
        distance_duration['duration'] = distance_duration_values.get('duration', 1000000) / 60
        return distance_duration
